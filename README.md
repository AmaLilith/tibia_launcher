![![header.png](https://bitbucket.org/repo/4ALjko/images/1012940556-header.png)](https://bitbucket.org/repo/9ALGrM/images/1923731031-header2.png)

![Launcher.png](https://bitbucket.org/repo/9ALGrM/images/3060633755-Launcher.png)

##	Keep multiply different set of hotkeys for c++ client.
* You can keep up to 4 diffrent set of hotkeys 1 for each vocation
##	Back up your map files!
* Simple and fast method for backing up your map files.



[![download.png](https://bitbucket.org/repo/4ALjko/images/33383977-download.png)](https://bitbucket.org/AmaLilith/tibia_launcher/get/master.zip)

----

## How to use
* Download and uncompress Tibia Map Converter anywhere.
* Run it and select what class you want to play.
* Log in on Tibia and **leave the launcher window open all time**, set your hotkeys for the vocation you selected, close tibia and repeat for all vocations.
* The window should auto close when you close Tibia that's when it saves the hotkeys and the reason you must leave it open

----

## FAQ
### Why do i need to have the launcher window open all time while Tibia is running?
* It's cause otherwise it wont save your specific vocation hotkeys. It saves them when you close Tibia the launcher window will close automatically when done.
### What is the ´cecho.exe´ inside tools folder?
* it's the exe needed to make the colors for version 4.0+.
### How do i backup my map files?
* You need to have either [Winrar](http://www.rarlab.com/download.htm) or [7-Zip Command line](http://www.7-zip.org/download.html). If its installed correctly the option should come up in the launcher!
* **Note:** If you use 7-Zip the 7za.exe needs to be placed in your windows folder!

----
#Version History

	7-Jul-2014 | Version 4.0
		* Added color coding for nicer looks
		* Added get admin function in the script to avoid failure if user doesnt run it as admin
	7-Jul-2014 | Version 3.0
		* Initial Release