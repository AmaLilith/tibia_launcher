::Tibia Launcher
::Version 4.0
::Created by Lilith at TibiaMaps.org
::GET ADMIN START
:: BatchGotAdmin
REM ==> Check for permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM ==> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"="
    echo UAC.ShellExecute "%~s0", "%params%", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )
    pushd "%CD%"
    CD /D "%~dp0"
::GET ADMIN END

setlocal enabledelayedexpansion
set STARTDIR=%CD%
set BackupFolder=%USERPROFILE%\Backup\Tibia\

:START
@ECHO off
COLOR 08
TITLE Tibia Launcher v.4.0
MODE CON:COLS=80 LINES=35
::browses to the directory the bat file is located in
  cd /d "%STARTDIR%"

  
  ::Fixes problems with writing the date in the automap zip file
  :: Extract date fields - language dependent
  for /f "tokens=1-4 delims=/-. " %%i in ('date /t') do (
        set v1=%%i& set v2=%%j& set v3=%%k
        if "%%i:~0,1%%" gtr "9" (set v1=%%j& set v2=%%k& set v3=%%l)

        for /f "skip=1 tokens=2-4 delims=(-)" %%m in ('echo.^|date') do (
            set %%m=!v1!& set %%n=!v2!& set %%o=!v3!
    )
  )  
:: Final set for language independency (English and Portuguese - maybe works for Spanish and French)
  set year=%yy%%aa%
  set month=%mm%
  set day=%dd%
  set fixdate=%year%-%month%-%day%
::Name of the Automap Backup zip files.
  set BackupZip=Automap - {%fixdate%}

::Creating the settings files if they do not exist
  if not exist "Settings.ini" ECHO [Custom Paths]>"Settings.ini"
  set INIFILE="%~dp0Settings.ini"
::set Game folder from Settings.ini
  call:getvalue %INIFILE% "CustomGameFolder" "" CustomGameFolder
  
::set Data folder from Settings.ini
  call:getvalue %INIFILE% "CustomDataFolder" "" CustomDataFolder

::Default backup folder
  
::set Backup folder from Settings.ini
  call:getvalue %INIFILE% "BackupFolder" "" BackupFolder
  
  
::set Costum WinRAR folder from Settings.ini
  call:getvalue %INIFILE% "CustomWinRARFolder" "" CustomWinRARFolder
  
::check for 7zip/winrar commandline version
  set ArchiveManager=no
  ::winrar 32bit
  if exist "%ProgramFiles(X86)%\WinRAR\" (set ArchiveManager=yes
  set path="%ProgramFiles(x86)%\WinRAR\";
  set sevenzip=no
  set winrarx64=no
  set winrarx86=yes
  set winrarcostum=no
  )
  ::winrar 64bit
  if exist "%ProgramFiles%\WinRAR\" (set ArchiveManager=yes
  set path="%ProgramFiles%\WinRAR\";
  set sevenzip=no
  set winrarx64=yes
  set winrarx86=no
  set winrarcostum=no
  )
  ::costum winrar
  if exist "%CustomWinRARFolder%" (set ArchiveManager=yes
  set path="%CustomWinRARFolder%";
  set sevenzip=no
  set winrarx64=no
  set winrarx86=no
  set winrarcostum=yes
  )
  ::7zip 32bit/64bit
  if exist "%systemroot%\7za.exe" (set ArchiveManager=yes
  set sevenzip=yes
  set winrarx64=no
  set winrarx86=no
  set winrarcostum=no
  ) 

::Check for Tibia folder.
  if exist "%ProgramFiles(X86)%\Tibia\" set TibiaFolder=%ProgramFiles(X86)%\Tibia\
  if exist "%ProgramFiles%\Tibia\" set TibiaFolder=%ProgramFiles%\Tibia\
  if exist "%CustomGameFolder%" set TibiaFolder=%CustomGameFolder%

::Check for Tibia data folder.
  if exist "%CustomDataFolder%" set DataFolder=%CustomDataFolder%
  if not exist "%CustomDataFolder%" set DataFolder=%AppData%\Tibia\
  cls
::Prints all text into window when the file is started.
  "%STARTDIR%"\tools\cecho Tibia Launcher v4.0                             Made by: {0A}Lilith ^@ TibiaMaps.org{#}& echo.
  ECHO ================================================================================
  ECHO Current used folders:& echo.
  "%STARTDIR%"\tools\cecho Tibia Game Folder: ^"{06}%TibiaFolder%{#}^"& echo.
  "%STARTDIR%"\tools\cecho Tibia Data Folder: ^"{06}%DataFolder%{#}^"& echo.
  if "%ArchiveManager%"=="yes"  ("%STARTDIR%"\tools\cecho Backup Folder:     ^"{06}%BackupFolder%{#}^"& echo.)
  ECHO ================================================================================
  ECHO Welcome, %USERNAME%& echo.
  ECHO 께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께
  ECHO �                                                                    �
  ECHO � MENU                                                               �
  ECHO �                                                                    �
  "%STARTDIR%"\tools\cecho � [{0A}1{#}] Knight Hotkeys                                                 �& echo.
  "%STARTDIR%"\tools\cecho � [{0A}2{#}] Paladin Hotkeys                                                �& echo.
  "%STARTDIR%"\tools\cecho � [{0A}3{#}] Druid Hotkeys                                                  �& echo.
  "%STARTDIR%"\tools\cecho � [{0A}4{#}] Sorcerer Hotkeys                                               �& echo.
  ECHO �--------------------------------------------------------------------�
  "%STARTDIR%"\tools\cecho � [{0A}5{#}] Game Master Client ^"Can't be used for playing^"                 �& echo.
  ECHO �--------------------------------------------------------------------�
  "%STARTDIR%"\tools\cecho � [{0A}6{#}] Open Tibia data folder ^"Containing .map files and config file^" �& echo.
  if "%ArchiveManager%"=="yes" ("%STARTDIR%"\tools\cecho � [{0A}7{#}] Backup Map Files ^"{06}Automap - {{YYYY-MM-DD.zip}{#}^"                  �& echo.)
  ECHO �--------------------------------------------------------------------�
  if "%ArchiveManager%"=="yes" ("%STARTDIR%"\tools\cecho � [{0A}8{#}] Options ^"Edit paths for Tibia/Data/Backup Folder^"              �& echo.) else ("%STARTDIR%"\tools\cecho � [{0A}7{#}] Options {{Edit paths for Tibia/Data Folder}                     �& echo.)
  ECHO �--------------------------------------------------------------------�
  if "%ArchiveManager%"=="yes" ("%STARTDIR%"\tools\cecho � [{0C}^<^{#}] Exit                                                           �& echo.) else ("%STARTDIR%"\tools\cecho � [{0A}8{#}] {0C}Exit{#}                                                           �& echo.)
  ECHO �                                                                    �
  ECHO 께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께께 
  ECHO.
  
::Creating the menu options.
  set /p choice="Enter your choice: "
    if "%choice%"=="1" (set cfg="Knight.cfg" & goto HOTKEYS)
    if "%choice%"=="2" (set cfg="Paladin.cfg" & goto HOTKEYS)
    if "%choice%"=="3" (set cfg="Druid.cfg" & goto HOTKEYS)
    if "%choice%"=="4" (set cfg="Sorcerer.cfg" & goto HOTKEYS)
    if "%choice%"=="5" (goto GM)
    if "%choice%"=="6" (goto DATAFOLDER)
    if "%ArchiveManager%"=="yes" (if "%choice%"=="7" goto BACKUP)
    if "%ArchiveManager%"=="no" (if "%choice%"=="7" goto OPTIONS)
    if "%ArchiveManager%"=="yes" (if "%choice%"=="8" goto OPTIONS)
    if "%ArchiveManager%"=="no" (if "%choice%"=="<" goto EOF)
    if "%ArchiveManager%"=="yes" (if "%choice%"=="<" goto EOF)
    if "%choice%" gtr 8 (goto START)
  ECHO.
  goto START

  
::Option 1-4
:HOTKEYS
::Replacing Tibia.cfg with the chosen config file. (Example replaces Tibia.cfg with Knight.cfg)
  cd /d "%DataFolder%"
  if exist "%cfg%" (copy /y "%cfg%" "Tibia.cfg")
  if exist "%cfg%" (MODE CON:COLS=80 LINES=12) else (MODE CON:COLS=80 LINES=14)
  cls
::Printing a messange in the window about whats going on.
  ECHO.
  ECHO ================================================================================
  cd /d "%DataFolder%"
  if exist "%cfg%" ("%STARTDIR%"\tools\cecho {0A}%cfg% Loaded successfully.{#}& echo.& echo.) else ("%STARTDIR%"\tools\cecho {0C}%cfg% did not exist.{#} & ECHO. & ECHO Please set your hotkeys in Tibia.& echo.)
  if exist "%cfg%" (ECHO It will automatically update %cfg% & ECHO with your current hotkeys when you exit Tibia!& echo.) else (ECHO It will automatically create %cfg% & ECHO with your current hotkeys when you exit Tibia!& echo.)
  "%STARTDIR%"\tools\cecho Leave this window open, it will automatically close when you close Tibia.& echo.& echo.
  "%STARTDIR%"\tools\cecho {0E}/{#}{0C}{\u0021}{#}{0E}\{#} {0E}If you close this window your hotkeys wont update when you close Tibia.{#} {0E}/{#}{0C}{\u0021}{#}{0E}\{#}& echo.
  ECHO ================================================================================
::Starting Tibia.
  cd /d "%TibiaFolder%"
  "%TibiaFolder%Tibia.exe" path "%DataFolder%"
::This happends when you exit Tibia.
::Replacing the chosen config with with Tibia.cfg to keep your hotkeys you load up to date. (Example replaces Knight.cfg with Tibia.cfg)
  cd /d "%DataFolder%"
  if exist "Tibia.cfg" (copy /y "Tibia.cfg" "%cfg%")
  goto:EOF

  
::Option 5
:GM
::Starting Tibia with the Gamemaster launch option.
  cls
  MODE CON:COLS=80 LINES=10
  ECHO ================================================================================
  ECHO Running Tibia with Gamemaster launch option...& echo.
  "%STARTDIR%"\tools\cecho {0E}/{#}{0C}{\u0021}{#}{0E}\{#} Gamemaster client cannot be used for playing! {0E}/{#}{0C}{\u0021}{#}{0E}\{#}& echo.
  ECHO But it can be used to watch Tibiacast while you play on normal client.& echo.
  ECHO ================================================================================
  cd /d "%TibiaFolder%"
  "%TibiaFolder%Tibia.exe" "gamemaster" path "%DataFolder%"
  goto:EOF

  
::Option 6
:DATAFOLDER
::Opens the Tibia data folder.
  explorer "%DataFolder%"
  goto:EOF
  
  
::Option 7 if 7zip/winrar exist
:BACKUP
::Archives the Automap folder and moves the zip file into the set backup folder.
  if not exist "%BackupFolder%" (md "%BackupFolder%")
  cd /d "%DataFolder%"
  if "%winrarx86%"=="yes" (winrar a -afzip -r "%BackupZip%.zip" "Automap")
  if "%winrarx64%"=="yes" (winrar a -afzip -r "%BackupZip%.zip" "Automap")
  if "%winrarcostum%"=="yes" (winrar a -afzip -r "%BackupZip%.zip" "Automap")
  if "%sevenzip%"=="yes" (7za a -tzip "%BackupZip%" "Automap")
  move /y "%BackupZip%.zip" "%BackupFolder%"
  MODE CON:COLS=80 LINES=5
  cls
  if exist "%BackupFolder%%BackupZip%.zip" (ECHO Backup Complete & ECHO %BackupZip%.zip Successfully moved to: & ECHO %BackupFolder%& echo.) else (ECHO Backup Failed & ECHO Make sure you have set a valid backup path& echo.)
  pause
  goto:START
  
  
  ::Option 8 or 7 depends on if 7zip/winrar exist or not
:OPTIONS
::clear all texts and makes a new meny {Options}
  MODE CON:COLS=80 LINES=13
  cls
  ECHO 께께께께께께께께께께께께께께께�
  ECHO �                             �
  ECHO � MENU                        �
  ECHO �                             �
  "%STARTDIR%"\tools\cecho � [{0A}1{#}] Edit Tibia folder path  �& echo.
  "%STARTDIR%"\tools\cecho � [{0A}2{#}] Edit Data folder path   �& echo.
  "%STARTDIR%"\tools\cecho � [{0A}3{#}] Edit WinRAR folder path �& echo.
  if "%ArchiveManager%"=="yes" ("%STARTDIR%"\tools\cecho � [{0A}4{#}] Edit Backup folder path �& echo.)

  ECHO �-----------------------------�
  if "%ArchiveManager%"=="yes" ("%STARTDIR%"\tools\cecho � [{0C}5{#}] Back                    �& echo.) else ("%STARTDIR%"\tools\cecho � [{0C}3{#}] Back                    �& echo.)
  ECHO �                             �
  ECHO 께께께께께께께께께께께께께께께�& echo.
  set /p choice="Enter your choice: "
    if "%choice%"=="1" (goto EDITTIBIAFOLDER)
    if "%choice%"=="2" (goto EDITDATAFOLDER)
    if "%choice%"=="3" (goto EDITWINRARFOLDER)
    if "%ArchiveManager%"=="yes" (if "%choice%"=="4" goto EDITBACKUPFOLDER)
    if "%ArchiveManager%"=="no" (if "%choice%"=="5" goto START)
    if "%ArchiveManager%"=="yes" (if "%choice%"=="5" goto START)
    if "%choice%"=="<" (goto START)
    if "%choice%" gtr 9 (goto OPTIONS)
  ECHO.
  goto OPTIONS

::option 8,1
:EDITTIBIAFOLDER
  MODE CON:COLS=80 LINES=14
  cls
  ECHO ================================================================================
  ECHO   Please enter your path to your Tibia folder ending with a \& echo.
  "%STARTDIR%"\tools\cecho   Example: {06}C:\Games\Tibia\{#}& echo.
  "%STARTDIR%"\tools\cecho   Write {0C}^<{#} to go back.& echo.
  ECHO ================================================================================& echo.
  "%STARTDIR%"\tools\cecho Current Game folder is: ^"{06}%CustomGameFolder%{#}^"& echo.
  set /p EditFolderPath="Enter Tibia folder path: "
  if "%EditFolderPath%"=="<" (goto OPTIONS)
  set CustomGameFolder=%EditFolderPath%
  ECHO [Custom Paths]>"Settings.ini" & set CustomGameFolder>>"settings.ini" & set CustomDataFolder>>"settings.ini" & set BackupFolder>>"settings.ini" & set CustomWinRARFolder>>"settings.ini"
  MODE CON:COLS=80 LINES=5
  cls
  ECHO Tibia folder path changed successfully to:
  ECHO.
  ECHO %CustomGameFolder%
  ECHO.
  pause
  goto START
::Option 8,2
:EDITDATAFOLDER
  mode con:lines=14
  cls
  ECHO ================================================================================
  ECHO   Please enter your path to your Tibia folder ending with a \& echo.
  "%STARTDIR%"\tools\cecho   Example: {06}C:\Games\Tibia\Data Folder\{#}& echo.
  "%STARTDIR%"\tools\cecho   Write {0C}^<{#} to go back.& echo.
  ECHO ================================================================================& echo.
  "%STARTDIR%"\tools\cecho Current Data folder is: ^"{06}%CustomDataFolder%{#}^"& echo.
  set /p EditFolderPath="Enter data folder path: "
  if "%EditFolderPath%"=="<" (goto OPTIONS)
  set CustomDataFolder=%EditFolderPath%
  ECHO [Custom Paths]>"Settings.ini" & set CustomGameFolder>>"settings.ini" & set CustomDataFolder>>"settings.ini" & set BackupFolder>>"settings.ini" & set CustomWinRARFolder>>"settings.ini"
  MODE CON:COLS=80 LINES=5
  cls
  ECHO Data folder path changed successfully to:& echo.
  ECHO %EditFolderPath%& echo.
  pause
  goto START
    ::Option 8,3
  :EDITWINRARFOLDER
  mode con:lines=14
  cls
  ECHO ================================================================================
  ECHO   Please enter your path to your Winrar folder ending with a \& echo.
  "%STARTDIR%"\tools\cecho   Example: {06}C:\WinRAR\{#}& echo.
  "%STARTDIR%"\tools\cecho   Write {0C}^<{#} to go back.& echo.
  ECHO ================================================================================& echo.
  "%STARTDIR%"\tools\cecho Current Data folder is: ^"{06}%CustomWinRARFolder%{#}^"& echo.
  set /p EditFolderPath="Enter data folder path: "
  if "%EditFolderPath%"=="<" (goto OPTIONS)
  set CustomWinRARFolder=%EditFolderPath%
  ECHO [Custom Paths]>"Settings.ini" & set CustomGameFolder>>"settings.ini" & set CustomDataFolder>>"settings.ini" & set BackupFolder>>"settings.ini" & set CustomWinRARFolder>>"settings.ini"
  MODE CON:COLS=80 LINES=5
  cls
  ECHO Data folder path changed successfully to:& echo.
  ECHO %EditFolderPath%& echo.
  pause
  goto START
::Option 8,4
:EDITBACKUPFOLDER
  mode con:lines=14
  cls
  ECHO ================================================================================
  ECHO   Please enter your path to your Backup folder ending with a \& echo.
  "%STARTDIR%"\tools\cecho   Example: {06}C:\BACKUPS\{#}& echo.
  "%STARTDIR%"\tools\cecho   Write {0C}^<{#} to go back.& echo.
  ECHO ================================================================================& echo.
  "%STARTDIR%"\tools\cecho Current Data folder is: ^"{06}%BackupFolder%{#}^"& echo.
  set /p EditFolderPath="Enter data folder path: "
  if "%EditFolderPath%"=="<" (goto OPTIONS)
  set BackupFolder=%EditFolderPath%
  ECHO [Custom Paths]>"Settings.ini" & set CustomGameFolder>>"settings.ini" & set CustomDataFolder>>"settings.ini" & set BackupFolder>>"settings.ini" & set CustomWinRARFolder>>"settings.ini"
  MODE CON:COLS=80 LINES=5
  cls
  ECHO Data folder path changed successfully to:& echo.
  ECHO %EditFolderPath%& echo.
  pause
  goto START
  
:getvalue
:: This function reads a value from an INI file and stored it in a variable
:: %1 = name of ini file to search in.
:: %2 = search term to look for
:: %3 = group name (not currently used)
:: %4 = variable to place search result
FOR /F "eol=; eol=[ tokens=1,2* delims==" %%i in ('findstr /b /l /i %~2= %1') DO set %~4=%%~j
goto:eof


  
::Option 9 or 8 depends on if 7zip exist or not
:EOF
  endlocal
::Closing the launcher.
  EXIT